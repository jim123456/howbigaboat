<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>How big a boat?</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato|Pacifico" rel="stylesheet">
        <!-- Styles -->
        <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">        
        <style>
            html{
                background: url('https://images.unsplash.com/photo-1518528057367-d8618b763ca0?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6a533d9dc42b1fbbec6367ce4a03fbfc&auto=format&fit=crop&w=693&q=80') no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            body {
                color: #636b6f;
                font-family: 'Lato', sans-serif;
                font-weight: 200;
                margin: 0;
            }

            .bigtitle {
                font-family: 'Pacifico', cursive;
                color: white;
            }

            .debugborder {
                border-style: solid; 
                border-color:red; 
                border-width: 2px;
            }

            /* Flex container */
            @media(max-width: 767px){
                #calculator{
                    display: flex;   
                    flex-direction:column;  
                }
            }

            @media(min-width: 768px){
                #calculator{
                    display: flex;
                }
            }

            /* Large letters */
            @media(max-width: 599px){
                .large-letters{
                    font-size: 50px;    
                }
            }

            @media(min-width: 600px){
                .large-letters{
                    font-size: 60px;    
                }
            }

            @media(min-width: 768px){
                .large-letters{
                    font-size: 70px;    
                }
            }

            @media(min-width: 992px){
                .large-letters{
                    font-size: 80px;    
                }
            }

            @media(min-width: 1200px){
                .large-letters{
                    font-size: 80px;    
                }
            }
        </style>
    </head>
    <body>
       <div class="bg">
       
            <div id="bigtitle" 
                class="container mx-auto bigtitle large-letters pt-10 text-center mb-20">
                How big a boat?
            </div>
       
            <!-- Vue app -->
            <div id="mute"></div>
            <div id="app">            
                <calculator-component></calculator-component>
            </div>
    
       </div>
    <script src="js/app.js"></script>
    </body>
</html>
