<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculateController extends Controller
{
     /**
      * Calculates horsepower based on calculator inputs
      *
      * @param Request $request
      * @return float $horsePower
      */
    public function calculate(Request $request)
    {
        $length = $request->length;
        $angle = ($request->angle)/10;
        $disp = $request->disp;

        $slRatio = ($angle *-0.2) + 2.9;
        $cw = 0.8+ (0.17 * $slRatio);
        $knots = $slRatio * sqrt($length);

        $horsepower = ($disp/1000) * (( $knots / ($cw*sqrt($length)) ) ** 3);  
        // (Disp/1000) * [Kts / (Cw * HL^½)]^3
        return $horsepower;
    }
}
